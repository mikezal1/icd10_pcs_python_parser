#! /usr/bin/python

#Import The Requirements
import xml.etree.cElementTree as ET
import sqlite3
import yaml
import json


class XMLNode(object):

       
    def __unicode__(self):
      return unicode(self.data)

    def __repr__(self):
        return self.__unicode__()
  
    def export_data(self,format = 'django'):

        if format == 'django':
          return self.fixture
        elif format == 'object':
          return self.data
        elif format == 'sql': 
          print("Will be implemented shortly. Currently returns None")
          return None
        else:
          print("Falling back to default. Exporting as a Django fixture in JSON")
          return self.data


    

class Definition(XMLNode):


    def __init__(self, definition, parent):

      self.index = self.__hash__()
      self.tag = 'definition'
      self.definition = definition
      self.parent = parent
      self.text = getattr( self.definition,'text', None)
      self.data = {'tag': self.tag, 
                   'parent': self.parent, 
                   'text': self.text or " " , 
                   'definition': self.definition or " " }
      self.fixture = {'model': 'icd10_pcs.Definition', 
                      'fields': {'index': self.index, 
                                 'text': self.text,
                                 'fk': self.parent.index 
                                },
                      'pk': self.index
                     }
     


class Title(XMLNode):


    def __init__(self, title, parent):

      self.index = self.__hash__()

      self.tag = 'title'
      self.title = title
      self.parent  = parent
      self.text = getattr(self.title,'text', None)
      self.data = {'tag': self.tag, 
                   'parent': self.parent, 
                   'title': self.title or " " , 
                   'text': self.text or " "  }
      self.fixture = {'model': 'icd10_pcs.Title', 
                      'fields': {'index': self.index,
                                 'text': self.text,
                                 'fk': self.parent.index
                                 },
                      'pk': self.index
                     }



class Label(XMLNode):


    def __init__(self, label, parent):

      self.index = self.__hash__()
      self.tag = 'label'
      self.label = label
      self.parent = parent
      self.text = getattr(self.label,'text', None)
      self.code = getattr(self.label, 'get', None)('code')
      self.data = {'tag': self.tag, 
                   'parent': self.parent, 
                   'label': self.label or " " , 
                   'text': self.text or " " , 
                   'code': self.code or " "  }
      self.fixture = {'model': 'icd10_pcs.Label', 
                      'fields': {'index': self.index,
                                 'fk': self.parent.index,
                                 'text': self.text,
                                 'code': self.code 
                                },
                      'pk': self.index
                     }

 

class Axis(XMLNode):


    def __init__(self, axis, parent):

       self.index = self.__hash__()
       self.tag = 'axis'
       self.axis = axis
       self.parent = parent
       self.values = getattr(self.axis,'get',None)('values')
       self.position = getattr(self.axis,'get',None)('pos')
       self.titles = []
       self.labels = []
       self.definitions = []

       for label in self.axis.iter('label'):
           self.labels.append(Label(label, parent = self))
       
       if self.axis.iter('definition') is not None:
          for definition in self.axis.iter('definition'):
            self.definitions.append(Definition(definition, parent = self))

       if self.axis.iter('title') is not None:
          for title in self.axis.iter('title'):
            self.titles.append(Title(title, parent = self))
       else:
          self.titles = None

       self.data = {'parent': self.parent, 
                    'tag': self.tag, 
		    'axis': self.axis, 
                    'titles': self.titles or " " , 
                    'labels': self.labels, 
                    'definitions': self.definitions or " ", 
                    'position': self.position or " " }
  
       self.fixture = {'model': 'icd10_pcs.Axis', 
                       'fields': {'index': self.index, 
                                 'positions': self.position or " ",
                                  'values': self.values or " ",
                                  self.parent.tag +'_fk': self.parent.index 
                                 },
                       'pk': self.index
                       }
       
class PcsRow(XMLNode):
    

    def __init__(self, row, parent):

       self.index = self.__hash__()

       self.tag = 'pcsRow'
       self.row = row
       self.parent = parent
       self.codes = row.get('codes')
       self.axis = []
       for axis in self.row.iter('axis'):
          self.axis.append(Axis(axis, parent = self))

       self.data = {'parent': self.parent, 
                    'tag': self.tag, 
                    'axis': self.axis or " ", 
                    'codes': self.codes or " " }

       self.fixture = {'model': 'icd10_pcs.PcsRow', 
                        'fields': {'index': self.index, 'fk': self.parent.index },
                        'pk': self.index
                       }


class PcsTable(XMLNode):


    def __init__(self, table, parent):

        self.index = self.__hash__()

        self.tag = 'pcsTable'
        self.table = table
        self.parent = parent
        self.axis = []
        self.pcs_rows = []

        for axis in  table.iter('axis'):
           self.axis.append(Axis(axis, parent = self))

        for row in self.table.iter('pcsRow'):
           self.pcs_rows.append(PcsRow(row, parent = self))

        self.data = {'parent': self.parent, 
                     'tag': self.tag, 
                     'axis': self.axis or " " , 
                     'rows': self.pcs_rows or " " }

        self.fixture = {'model': 'icd10_pcs.PcsTable', 
                        'fields': {'index': self.index, 'fk': self.parent.index },
                        'pk': self.index
                       }
                        
   

class RootXML(XMLNode):
    

    def __init__(self, path=None):
 
        self.index = self.__hash__()

        if not path:
          self.path = 'icd10pcs_tabular_2014.xml'
        else:
          self.path = path

        try:
          self.tree = ET.parse(self.path)
        except (IOError):
          raise IOError

        self.root = self.tree.getroot()
        self.pcs_tables = []
        self.tag = "xml"
        self.data = {'path': self.path,
                      'tag': self.tag,
                      'tables': self.pcs_tables
                    }
        self.fixture = { 'model': 'icd10_pcs.RootXML',
                         'fields': { 'index': self.index, 'fk': None, 'path': self.path },
                         'pk': self.index 
                       }
        self._build_pcs_tables()

    def _build_pcs_tables(self):
        all_pcs_tables = self.root.iter('pcsTable')
        for pcs_table in all_pcs_tables:
           self.pcs_tables.append(PcsTable(pcs_table, parent = self))




def main():

   # Initializing the RootXML class initializes other classes 
   #    and collects data as instances and attributes of instances
   # With Chained initialisation of the PcsTables, PcsRow, Axis etc... 
   #    the pcs_tables attribute can hold all the data for the XML in object form. 

   # Can this be used to construct / serialize into JSON / YAML / SQL without knowing what tag names are ? 
   # All data gets accumulated inside this instance attribute
   # Now question is to construct a serializer that retrieves and presents 
   #    data the way we want into JSON / YAML
   # Of course performance is another question....

   rootXML=RootXML() 

   # This will print the label PCS Code
   # print(rootXML.pcs_tables[0].pcs_rows[0].axis[0].labels[0].code)

   # This will print label PCS Text
   # print(rootXML.pcs_tables[0].pcs_rows[0].axis[0].labels[0].text)

   initial_data = []
   initial_data.append(rootXML.export_data())
  
   for table in rootXML.pcs_tables:
      initial_data.append(table.export_data())
      try:
	      for axis in table.axis:
		initial_data.append(axis.export_data())
		for title in axis.titles:
		  initial_data.append(title.export_data())
		for label in axis.labels:
		  initial_data.append(label.export_data())
		for definition in axis.definitions:
		  initial_data.append(definition.export_data())
	 
	      for row in table.pcs_rows:
		initial_data.append(row.export_data())

		for axis in row.axis:
		  initial_data.append(axis.export_data())
		  for title in axis.titles:
		    initial_data.append(title.export_data())
		  for label in axis.labels:
		    initial_data.append(label.export_data())
		  for definition in axis.definitions:
		    initial_data.append(definition.export_data())

      except Exception as ex:
               raise ex

   with open('initial_pcs_data_class_based.json','w') as initial_data_file:
        json_data = json.dumps(initial_data, encoding = 'utf-8', sort_keys = True, indent = 4)
        initial_data_file.write(json_data)
        initial_data_file.close()

   #with open('initial_pcs_data_class_based.json','rb') as initial_data_file:
    #    json_data = json.load(initial_data_file)
    #    print json_data      



if __name__ == '__main__':

    main()
