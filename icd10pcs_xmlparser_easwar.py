#! /usr/bin/python

#Import The Requirements
#from   elementtree import ElementTree as et
import xml.etree.ElementTree as ET
import sqlite3
import yaml
import json

#Define Constatnts

icd10pcs_yaml = file('icd10pcs.yaml','w')
icd10pcs_json = file('icd10pcs.json','w')


#Parse the XML and get the Tree Element
tree = ET.parse('icd10pcs_tabular_2014.xml')
root = tree.getroot()
print "Parsed the XML File ..."
print "Root's Tag is: ", root.tag
print "Root's attribute is: ", root.attrib


#Setup DB Connection Object and Cursor
print "Setting up DB connection and Cursor"
db_conn = sqlite3.connect('icd10pcs.db')
db_conn.execute("PRAGMA foreign_keys = ON")
db_cursor = db_conn.cursor()


#Define and Open Files for Reading and Writing
print "Setting up Files for Reading and Writing.."
f          = open('icd10pcs_tabular_2014.xml','r')
w          = open('icd10pcs.txt','w')
dummy_file = open('dummy_file_icd10pcs.txt','w')


# Set up the JSON file
initial_json_data     = open('initial_pcs_data.json','w')

json_list             = []
section_json_list     = []
body_system_json_list = []
operation_json_list   = []


#Get all PCS tables
all_pcs_tables = root.iter('pcsTable')

section_list     = []
body_system_list = []
operation_list   = []

def create_pcs_table():

  db_cursor.execute(""" CREATE TABLE IF NOT EXISTS
                          pcsTable( sec_id integer,
                                    section text,
                                    body_system text,
                                    operation text,
                                    PRIMARY KEY(sec_id)
                          );
                    """
  )

  db_cursor.execute(""" CREATE TABLE IF NOT EXISTS
                          pcsRow(
                                pcsRow_id integer,
                                pcsTable_fk integer,
                                FOREIGN KEY(pcsTable_fk) REFERENCES pcsTable(sec_id),
                                PRIMARY KEY(pcsRow_id)
                          );
                    """
  )

  db_cursor.execute(""" CREATE TABLE IF NOT EXISTS
                          bodyPart(body_part text,
                                  pcsRow_fk integer,
                                  FOREIGN KEY(pcsRow_fk) REFERENCES pcsRow(pcsRow_id)
                          );	"""
  )
  db_cursor.execute(""" CREATE TABLE IF NOT EXISTS
                          approach(approach text,
                                   pcsRow_fk integer,
                                   FOREIGN KEY(pcsRow_fk) REFERENCES pcsRow(pcsRow_id)
                                   );
                    """
  )
  db_cursor.execute(""" CREATE TABLE IF NOT EXISTS
                          device(device text,
                                  pcsRow_fk integer,
                                    FOREIGN KEY(pcsRow_fk) REFERENCES pcsRow(pcsRow_id)
                          );
                    """
  )
  db_cursor.execute(""" CREATE TABLE IF NOT EXISTS
                          qualifier(qualifier text,
                                    pcsRow_fk integer,
                                    FOREIGN KEY(pcsRow_fk) REFERENCES pcsRow(pcsRow_id)
                          );
                    """
  )

  db_conn.commit()

  i =1
  rowNum = 1
  for pcsTable in all_pcs_tables:
    first_axis = pcsTable.findall('axis')[0]
    second_axis = pcsTable.findall('axis')[1]
    third_axis = pcsTable.findall('axis')[2]

    section     = first_axis.find('label').text
    body_system = second_axis.find('label').text
    operation   = third_axis.find('label').text

#    db_cursor.execute(""" INSERT INTO
#                            pcsTable
#                              VALUES(%d, '%s','%s','%s')
#                      """ %(i, section, body_system, operation)
#    )
#    db_conn.commit()

    pcsTable_json_dict={}
    pcsTable_json_dict['fields']={'section'     : section,
                                  'body_system' : body_system,
                                  'operation'   : operation,
                                  'sec_id'      : i
                                }
    pcsTable_json_dict['model'] = "icd10_pcs.PcsTable"
    pcsTable_json_dict['pk']    = int(i)
    json_list.append(pcsTable_json_dict)


    all_pcs_rows = pcsTable.findall('pcsRow')
    
    
    for pcs_row in all_pcs_rows:
      all_body_parts = pcs_row.findall('axis')[0].findall('label')
      all_approaches = pcs_row.findall('axis')[1].findall('label')
      all_devices    = pcs_row.findall('axis')[2].findall('label')
      all_qualifiers = pcs_row.findall('axis')[3].findall('label')
#      db_cursor.execute("""INSERT INTO pcsRow VALUES(%d,%d);""" %( rowNum, i))
#      db_conn.commit()
      
      pcsRow_json_dict={}
      pcsRow_json_dict['fields']={'pcsRow_id'     : rowNum,
                                  'pcsTable_fk' : i
                                 }
      pcsRow_json_dict['model'] = "icd10_pcs.PcsRow"
      pcsRow_json_dict['pk']    = int(rowNum)
      json_list.append(pcsRow_json_dict)
      
      print "Committed..", rowNum, " to DB"
      
      bp = 1
      app = 1
      dev = 1
      qua = 1
      
      for body_part in all_body_parts:
        body_part_text = body_part.text
#        db_cursor.execute("""INSERT INTO bodyPart VALUES('%s',%d);""" %(body_part_text, rowNum))
#        db_conn.commit()
        
        bodyPart_json_dict={}
        bodyPart_json_dict['fields']={"body_part" : body_part_text,
                                      'pcsRow_fk' : rowNum
                                  }
        bodyPart_json_dict['model'] = "icd10_pcs.BodyPart"
        bodyPart_json_dict['pk']    = int(bp)
        json_list.append(bodyPart_json_dict)
        
        bp += 1
        
        print "Committed..BODYPART", body_part_text, " to DB"
      
      for approach in all_approaches:
        approach_text = approach.text
#        db_cursor.execute("""INSERT INTO approach VALUES('%s',%d);""" %(approach_text, rowNum))
#        db_conn.commit()
        
        approach_json_dict={}
        approach_json_dict['fields']={"approach" : approach_text,
                                      'pcsRow_fk' : rowNum
                                  }
        approach_json_dict['model'] = "icd10_pcs.Approach"
        approach_json_dict['pk']    = int(app)
        json_list.append(approach_json_dict)
        
        app += 1
        
        print "Committed..APPROACH", approach_text, " to DB"
      
      for device in all_devices:
        device_text = device.text
#        db_cursor.execute("""INSERT INTO device VALUES('%s',%d);""" %(device_text, rowNum))
#        db_conn.commit()
        
        device_json_dict={}
        device_json_dict['fields']={"device" : device_text,
                                      'pcsRow_fk' : rowNum
                                  }
        device_json_dict['model'] = "icd10_pcs.Device"
        device_json_dict['pk']    = int(dev)
        json_list.append(device_json_dict)
        
        dev += 1
        
        print "Committed..DEVICE TEXT", device_text, " to DB"
      
      for qualifier in all_qualifiers:
        qualifier_text = qualifier.text
#        db_cursor.execute("""INSERT INTO qualifier VALUES('%s',%d);""" %(qualifier_text, rowNum))
#        db_conn.commit()
        
        qualifier_json_dict={}
        qualifier_json_dict['fields']={"qualifier" : qualifier_text,
                                      'pcsRow_fk' : rowNum
                                  }
        qualifier_json_dict['model'] = "icd10_pcs.Qualifier"
        qualifier_json_dict['pk']    = int(qua)
        json_list.append(qualifier_json_dict)
        
        qua += 1
        
        print "Committed..QUALIFIER TEXT", qualifier_text, " to DB"
      
      rowNum +=1
    i += 1      
    
    #if section not in section_list:
      #section_list.append(section)
    #print section_list
  
def dump_the_json():
  entire_fixture = json.dumps(json_list, indent = 4)
  initial_json_data.write(entire_fixture)
  initial_json_data.close()


if __name__ == '__main__':
  create_pcs_table()
  dump_the_json()
